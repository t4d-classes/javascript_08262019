import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CarHomeComponent } from './components/car-home/car-home.component';
import { ToolHeaderComponent } from './components/tool-header/tool-header.component';
import { CarFormComponent } from './components/car-form/car-form.component';
import { CarTableComponent } from './components/car-table/car-table.component';

@NgModule({
  declarations: [CarHomeComponent, ToolHeaderComponent, CarFormComponent, CarTableComponent],
  exports: [ CarHomeComponent ],
  imports: [
    CommonModule, ReactiveFormsModule,
  ]
})
export class CarToolModule { }
