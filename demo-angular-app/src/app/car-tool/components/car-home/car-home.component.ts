import { Component } from '@angular/core';

import { ICar } from '../../models/ICar';

@Component({
  selector: 'app-car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css']
})
export class CarHomeComponent {

  headerText = 'Car Tool';

  cars: ICar[] = [
    { id: 1, make: 'Ford', model: 'Fusion Hybrid', year: 2018, color: 'red', price: 24000 },
    { id: 2, make: 'Ford', model: 'GT-40', year: 2018, color: 'blue', price: 480000 },
  ];

  doAddCar(car: ICar) {

    this.cars = this.cars.concat({
      ...car,
      id: Math.max(...this.cars.map(c => c.id), 0) + 1,
    });

  }

  doDeleteCar(carId: number) {
    this.cars = this.cars.filter(c => c.id !== carId);
  }

}
