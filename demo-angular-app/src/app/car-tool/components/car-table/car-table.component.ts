import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ICar } from '../../models/ICar';

@Component({
  selector: 'app-car-table',
  templateUrl: './car-table.component.html',
  styleUrls: ['./car-table.component.css']
})
export class CarTableComponent implements OnInit {

  @Input()
  cars: ICar[] = [];

  @Output()
  deleteCar = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }


}
