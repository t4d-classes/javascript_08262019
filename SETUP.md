# Setup Computer for Applied JavaScript Class

Note: The word `terminal` refers to the Windows Command Prompt program on Windows and the Terminal program on macOS

## Install Node.js

1. Open a web browser and navigate to [https://nodejs.org](https://nodejs.org)

2. Download the most recent LTS version of Node.js

3. Run the installer and complete the installation for your system

4. To verify the install, open a new terminal window, then run the following command:

```
node -v
```

Ensure the version number matches the version you downloaded

> Helpful Hint: If you need to run multiple versions of Node.js on your system consider using [NVM-Windows](https://github.com/coreybutler/nvm-windows) (for Windows) and [NVM](https://github.com/nvm-sh/nvm) (for macOS)

## Verify NPM Connectivity

1. An important part of Node.js development is the ability to download and install NPM packages. Run the following command from a new terminal window to ensure you download NPM packages

```
npx create-react-app demo-app
```

If everything is successful you should see output that looks like this:

```
Success! Created demo-app at /Users/ericwgreene/git/t4d-classes/javascript_08262019/demo-app
Inside that directory, you can run several commands:

  npm start
    Starts the development server.

  npm run build
    Bundles the app into static files for production.

  npm test
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

We suggest that you begin by typing:

  cd demo-app
  npm start

Happy hacking!
```

2. Not required, but if you want, you can run the new React application

```
cd demo-app

npm start
```

The application should start and open in your default web browser

## Install Docker

1. Install Docker Desktop from the Docker web site

For Windows - [https://docs.docker.com/docker-for-windows/install/](https://docs.docker.com/docker-for-windows/install/)

For Mac - [https://docs.docker.com/docker-for-mac/install/](https://docs.docker.com/docker-for-mac/install/)

2. First, read the Docker installation instructions, then download the installer. Next, run the installer and complete any verification steps listed by Docker

## Run MongoDB in a Docker Container

1. Open a new terminal window, and run the following command:

```
docker pull mongo
```

It will take a few minutes to pull the Docker image from Docker Hub

2. Create a Docker Volume to persist the data:

```
docker volume create --name=mongodata
```

> Note: if you skip this step, the data in your MongoDB server will not be persisted between container stops and starts

3. Run the Mongo Docker image with the following command:

```
docker run --name mongo-server -v mongodata:/data/db -p 27017:27017 mongo mongod
```

> Note: the name can be anything you want, but once you create a container with a given name a new container cannot be created with the same name, to reuse the name the first container must first be removed, see instructions at the bottom of this document on how to do that

## Install Robo3T MongoDB Client

1. Download Robo 3T (not Studio 3T) from here: [https://robomongo.org/download](https://robomongo.org/download)

2. Run the installer and complete the installation for your system

3. Run Robo 3T, create a new connection. In the MongoDB Connections Popup, click the `Create` link in the upper left corner of the window

![Create Mongo Connection](./images/mongo-connections.png)

4. Ensure `Direct Connection` is selected. Name the connection whatever you would like. Ensure the `Address` is set to `localhost` and the `Port` is set to `27017`

![New Mongo Connection Settings](./images/new-mongo-connection.png)

5. Ensure the first checkbox in the `Authentication`, `SSH`, and `SSL` tabs is unchecked. No advanced options need to be set.

> For a real development or production database, authentication (and other security options) would be used but for this class it is unnecessary

1. Click the `Save` button.

![New Mongo Connection Settings](./images/save-mongo-connection.png)

7. Highlight the new connection in the list and click the `Connect` button.

![Save Mongo Connection](./images/updated-connections-list.png)

8. Ensure you see a Mongo server with the name you specified for the connection listed in the tree of connections.

![Mongo Connections Tree](./images/mongo-connections-tree.png)

## Complete the Setup

1. Exit Robo 3T

2. Hit Ctrl+C in the terminal where you started MongoDB

3. Close all terminal windows

## Future Work

1. If you decide to restart the MongoDB server, use the following commands

```
docker ps -al
```

2. Using the Container Id for the Mongo server from the previous step, run the following command

![Docker Container List](./images/container-list.png)

```
docker start <CONTAINER ID>
```

Now Robo 3T can be used to reconnect to the database

3. When you are done using the MongoDB container

```
docker stop <CONTAINER ID>
```

## Remove the MongoDB Container

1. If you want to remove the container to repeat the setup described above, run the following command:

```
docker rm <CONTAINER ID>
```