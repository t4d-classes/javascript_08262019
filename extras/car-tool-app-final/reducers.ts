import { combineReducers, Action } from 'redux';

import { ICar } from './models/ICar';

import {
  REFRESH_CARS_DONE_ACTION,
  IRefreshCarsDoneAction,
  EDIT_CAR_ACTION,
  CANCEL_CAR_ACTION,
  IEditCarAction,
} from './actions';

export const carsReducer = (cars: ICar[] = [], action: Action<string>) => {
  console.log(action);

  if (action.type === REFRESH_CARS_DONE_ACTION) {
    const refreshCarsDoneAction = action as IRefreshCarsDoneAction;
    return refreshCarsDoneAction.payload.cars;
  }

  return cars;
};

export type EditCarIdActionsUnion = IRefreshCarsDoneAction | IEditCarAction;

export const editCarIdReducer = (
  editCarId: number = -1,
  action: EditCarIdActionsUnion,
) => {
  if ([CANCEL_CAR_ACTION, REFRESH_CARS_DONE_ACTION].includes(action.type)) {
    return -1;
  }

  if (action.type === EDIT_CAR_ACTION) {
    const editCarAction = action as IEditCarAction;
    return editCarAction.payload.carId;
  }

  return editCarId;
};

export const carToolReducer = combineReducers({
  cars: carsReducer,
  editCarId: editCarIdReducer,
});
