import { Action } from 'redux';
import { ICar } from './models/ICar';

export const REFRESH_CARS_REQUEST_ACTION = 'REFRESH_CARS_REQUEST_ACTION';
export const REFRESH_CARS_DONE_ACTION = 'REFRESH_CARS_DONE_ACTION';

export const APPEND_CAR_REQUEST_ACTION = 'APPEND_CAR_REQUEST_ACTION';
export const REPLACE_CAR_REQUEST_ACTION = 'REPLACE_CAR_REQUEST_ACTION';
export const DELETE_CAR_REQUEST_ACTION = 'DELETE_CAR_REQUEST_ACTION';

export const EDIT_CAR_ACTION = 'EDIT_CAR_ACTION';
export const CANCEL_CAR_ACTION = 'CANCEL_CAR_ACTION';

export interface IRefreshCarsDoneAction
  extends Action<string> {
    payload: {
      cars: ICar[];
    };
  }

export const createRefreshCarsRequestAction:
  () => Action<string> =
    () => ({ type: REFRESH_CARS_REQUEST_ACTION });

export const createRefreshCarsDoneAction:
  (cars: ICar[]) => IRefreshCarsDoneAction =
    (cars) =>
      ({ type: REFRESH_CARS_DONE_ACTION, payload: { cars } });

export interface IEditCarAction
  extends Action<string> {
    payload: {
      carId: number;
    };
  }

export const createEditCarAction:
  (carId: number) => IEditCarAction =
    (carId) => ({
      type: EDIT_CAR_ACTION,
      payload: { carId },
    });

export const createCancelCarAction:
  () => Action<string> =
    () => ({
      type: CANCEL_CAR_ACTION,
    });

export interface IAppendCarRequestAction
  extends Action<string> {
    payload: {
      car: ICar;
    };
  }

export const createAppendCarRequestAction:
  (car: ICar) => IAppendCarRequestAction =
    (car) => ({
      type: APPEND_CAR_REQUEST_ACTION,
      payload: { car },
    });

export interface IReplaceCarRequestAction
  extends Action<string> {
    payload: {
      car: ICar;
    };
  }

export const createReplaceCarRequestAction:
  (car: ICar) => IReplaceCarRequestAction =
    (car) => ({
      type: REPLACE_CAR_REQUEST_ACTION,
      payload: { car },
    });

export interface IDeleteCarRequestAction
  extends Action<string> {
    payload: {
      carId: number;
    };
  }

export const createDeleteCarRequestAction:
  (carId: number) => IDeleteCarRequestAction =
    (carId) => ({
      type: DELETE_CAR_REQUEST_ACTION,
      payload: { carId },
    });
