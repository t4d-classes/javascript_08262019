import { createStore } from 'redux';

import { calcReducer } from './calcReducers';

export const calcStore = createStore(calcReducer);
