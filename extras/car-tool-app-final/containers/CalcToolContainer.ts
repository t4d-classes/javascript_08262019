import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { createAddAction, createSubtractAction } from '../calcActions';

import { CalcTool } from '../components/CalcTool';

export const CalcToolContainer = connect(
  ({ result }) => ({ result }),
  (dispatch: Dispatch) => bindActionCreators({
    onAdd: createAddAction,
    onSubtract: createSubtractAction,
  }, dispatch),
)(CalcTool);
