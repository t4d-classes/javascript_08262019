import { takeLatest, takeEvery, call, put } from 'redux-saga/effects';
import axios from 'axios';

import {
  REFRESH_CARS_REQUEST_ACTION,
  APPEND_CAR_REQUEST_ACTION,
  REPLACE_CAR_REQUEST_ACTION,
  DELETE_CAR_REQUEST_ACTION,
  createRefreshCarsRequestAction,
  createRefreshCarsDoneAction,
  IAppendCarRequestAction,
  IReplaceCarRequestAction,
  IDeleteCarRequestAction,
} from './actions';

export function* refreshCars() {
  const response = yield call(axios.get, 'http://localhost:3010/cars');

  yield put(createRefreshCarsDoneAction(response.data));
}

export function* appendCar(action: IAppendCarRequestAction) {
  yield call(axios.post, 'http://localhost:3010/cars', action.payload.car);

  yield put(createRefreshCarsRequestAction());
}

export function* replaceCar(action: IReplaceCarRequestAction) {
  yield call(
    axios.put,
    'http://localhost:3010/cars/' + encodeURIComponent(action.payload.car.id),
    action.payload.car,
  );

  yield put(createRefreshCarsRequestAction());
}

export function* deleteCar(action: IDeleteCarRequestAction) {
  yield call(
    axios.delete,
    'http://localhost:3010/cars/' + encodeURIComponent(action.payload.carId),
  );

  yield put(createRefreshCarsRequestAction());
}

export function* carsSaga() {
  yield takeLatest(REFRESH_CARS_REQUEST_ACTION, refreshCars);
  yield takeEvery(APPEND_CAR_REQUEST_ACTION, appendCar);
  yield takeEvery(REPLACE_CAR_REQUEST_ACTION, replaceCar);
  yield takeEvery(DELETE_CAR_REQUEST_ACTION, deleteCar);
}
