import React, { ChangeEvent } from 'react';

export interface ICalcToolProps {
  result: number;
  onAdd: (num: number) => void;
  onSubtract: (num: number) => void;
}

export interface ICalcToolState {
  numInput: number;
}

export class CalcTool extends React.Component<ICalcToolProps, ICalcToolState> {
  public state = {
    numInput: 0,
  };

  public change = ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
    this.setState({ numInput: Number(value) });
  }

  public render() {
    return (
      <form>
        <div>Result: {this.props.result}</div>
        <div>
          <label htmlFor="num-input">Input: </label>
          <input
            type="number"
            id="num-input"
            value={this.state.numInput}
            onChange={this.change}
          />
        </div>
        <button
          type="button"
          onClick={() => this.props.onAdd(this.state.numInput)}
        >
          +
        </button>
        <button
          type="button"
          onClick={() => this.props.onSubtract(this.state.numInput)}
        >
          -
        </button>
      </form>
    );
  }
}
