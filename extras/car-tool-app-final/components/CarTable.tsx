import { ICar } from '../models/ICar';

import { ViewCarRow } from '../components/ViewCarRow';
import { EditCarRow } from '../components/EditCarRow';

export interface ICarTableProps {
  cars: ICar[];
  editCarId: number;
  onEditCar: (carId: number) => void;
  onDeleteCar: (carId: number) => void;
  onSaveCar: (car: ICar) => void;
  onCancelCar: () => void;
}

export const CarTable = (props: ICarTableProps) => {

  return (
    <table>
      <thead>
        <tr>
          <th>Id</th>
          <th>Make</th>
          <th>Model</th>
          <th>Year</th>
          <th>Color</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {props.cars.map((car) => (car.id === props.editCarId
          ? <EditCarRow key={car.id} car={car}
              onSaveCar={props.onSaveCar} onCancelCar={props.onCancelCar} />
          : <ViewCarRow key={car.id} car={car}
              onEditCar={props.onEditCar} onDeleteCar={props.onDeleteCar} />
        ))}
      </tbody>
    </table>
  );
};

CarTable.defaultProps = {
  cars: [],
};
