import React from 'react';
import { ICar } from '../models/ICar';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';

export interface ICarToolProps {
  cars: ICar[];
  editCarId: number;
  onRefreshCars: () => void;
  onEditCar: (carId: number) => void;
  onCancelCar: () => void;
  onAppendCar: (car: ICar) => void;
  onDeleteCar: (carId: number) => void;
  onReplaceCar: (car: ICar) => void;
}

export class CarTool extends React.Component<ICarToolProps, {}> {

  constructor(props: ICarToolProps) {
    super(props);

    this.appendCar = this.appendCar.bind(this);
    this.deleteCar = this.deleteCar.bind(this);
    this.replaceCar = this.replaceCar.bind(this);
    this.editCar = this.editCar.bind(this);
    this.cancelCar = this.cancelCar.bind(this);
  }

  public componentDidMount() {
    this.props.onRefreshCars();
  }

  public appendCar(car: ICar) {
    this.props.onAppendCar(car);
  }

  public replaceCar(car: ICar) {
    this.props.onReplaceCar(car);
  }

  public editCar(carId: number) {
    this.props.onEditCar(carId);
  }

  public deleteCar(carId: number) {
    this.props.onDeleteCar(carId);
  }

  public cancelCar() {
    this.props.onCancelCar();
  }

  public render() {
    return <>
      <ToolHeader headerText="Car Tool App" />
      <CarTable cars={this.props.cars} editCarId={this.props.editCarId}
        onEditCar={this.editCar} onDeleteCar={this.deleteCar}
        onSaveCar={this.replaceCar} onCancelCar={this.cancelCar} />
      <CarForm buttonText="Add Car" onSubmitCar={this.appendCar} />
    </>;
  }
}
