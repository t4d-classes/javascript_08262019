import React from 'react';

export interface IToolHeaderProps {
  headerText: string;
}

export const ToolHeader = (props: IToolHeaderProps) => {

  return <header>
    <h1>{props.headerText}</h1>
  </header>;

};
