const MongoClient = require('mongodb').MongoClient;

// Connection URL
const url = 'mongodb://localhost:27017';


const getAllCars = (url, cb) => {


  const client = new MongoClient(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  client.connect((err) => {

    if (err) {
      console.log(err);
      return;
    }

    client
      .db("cartool")
      .collection('cars')
      .find({})
      .toArray((err, carDocs) => {

        if (err) {
          console.log(err);
          return;
        }

        client.close(err => {

          if (err) {
            console.log(err);
            return;
          }
    
          cb(carDocs);

        });

      });

  });

};


getAllCars(url, cars => {
  console.log(cars);
});
