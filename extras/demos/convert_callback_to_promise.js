const MongoClient = require('mongodb').MongoClient;

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'cartool';


const getMongoClient = (url) => {

  return new Promise((resolve, reject) => {

    const client = new MongoClient(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    client.connect((err) => {
      if (err) {
        reject(err);
      }

      resolve(client);
    });

  });
};

const closeMongoClient = (client) => {

  return new Promise((resolve, reject) => {

    client.close(err => {

      if (err) {
        reject(err);
        return;
      }

      resolve();
    });

  });

};

const getAllCarsFromMongo = (db) => {

  return new Promise((resolve, reject) => {

    db.collection('cars').find({}).toArray((err, carDocs) => {

      if (err) {
        reject(err);
        return;
      }

      resolve(carDocs);

    });

  });

}

const getAllCars = async (url) => {

  const client = await getMongoClient(url);
  const cars = await getAllCarsFromMongo(client.db("cartool"));

  await closeMongoClient(client);

  return cars;
};

getAllCars(url).then(cars => {
  console.log(cars);
});
