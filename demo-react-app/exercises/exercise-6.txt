Exercise 6

1. Create a new component named Car Table.

2. Move the car table in Car Tool to the new Car Table component.

3. Continue to maintain the cars state inside of Car Tool.

4. Replace the car table in Car Tool with the new Car Table component.

5. Ensure the delete and add functionality still works.

6. Ensure it works.