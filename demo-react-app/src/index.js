import React from 'react';
import { render } from 'react-dom';

import { ColorTool } from './components/ColorTool';
import { CarTool } from './components/CarTool';

const carList = [
  { id: 1, make: 'Toyota', model: 'Yaris', year: 2019, color: 'white', price: 17000 },
  { id: 2, make: 'Toyota', model: 'Sienna', year: 2004, color: 'blue', price: 15000 },
];

render(
  <>
    <ColorTool />
    <CarTool cars={carList} />
  </>,
  document.querySelector('#root'),
);

