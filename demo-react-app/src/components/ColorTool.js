import React from 'react';

export const ColorTool = () => {

  const colors = [ 'red', 'green', 'blue' ];

  return (
    <>
      <h1>Color Tool</h1>
      <ul>
        {colors.map((color, index) =>
          <li key={index}>{color}</li>)}
      </ul>
    </>
  );

};

