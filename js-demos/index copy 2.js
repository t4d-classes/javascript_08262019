const MongoClient = require('mongodb').MongoClient;

// Connection URL
const url = 'mongodb://localhost:27017';

const getAllCarsFromMongo = (client) => {
  return new Promise((resolve, reject) => {
    client
      .db("cartool")
      .collection('cars')
      .find({})
      .toArray((err, carDocs) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(carDocs);
      });    
  });
};

const closeMongo = (client, carDocs) => {
  return new Promise((resolve, reject) => {
    client.close(err => {
      if (err) {
        reject(err);
        return;
      }
      resolve(carDocs);
    });
  });
};

const getMongoConnection = url => {
  return new Promise((resolve, reject) => {
    const client = new MongoClient(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    client.connect((err) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(client);
    });
  });
};

const getAllCars = async (url) => {

  const client = await getMongoConnection(url);

  const cars = await getAllCarsFromMongo(client);

  await closeMongo(client);

  return cars;

};


getAllCars(url).then(cars => {
  console.log(cars);
});
