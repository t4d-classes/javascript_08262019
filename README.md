# Welcome to Applied JavaScript Class!

## Instructor

Eric Greene

## Schedule

Class:

- Monday - Friday: 9am to 5pm PDT (may get out a little early on Friday)

Breaks:

- Morning Break: 10:25am to 10:35am
- Lunch: 12pm to 1pm
- Afternoon Break #1: 2:15pm to 2:25pm
- Afternoon Break #2: 3:40pm to 3:50pm

## Course Outline

### JavaScript content will be included as needed in the class

- Day 1: JavaScript Language (ES2019, Patterns)
- Day 2: JavaScript Language (ES2019, Patterns)
- Day 3: Client-Side Libraries (Template, Components, Web Components)
- Day 4: Client-Side Libraries (continued), Node.js (Node, NPM, Express, MongoDB, Socket.IO)  
- Day 5: Node.js (continued), Docker

## Links

### Instructor's Resources

- [Wintellect](https://www.wintellect.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016
- [Free Udemy Class on React](https://www.udemy.com/getting-started-with-react)
